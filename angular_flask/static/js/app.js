//'use strict';
var App = angular.module('AngularFlask', ['ngTable', 'ngRoute']);

// Configure app.
App.config(['$routeProvider', '$locationProvider',
	function ($routeProvider, $locationProvider) {
		// Enable support for HTML5 browsers.
		$locationProvider.html5Mode({
			enabled: true,
			requireBase: false
		});
	}]);

// Define body controller.
App.controller('bodyController', ['$scope', '$http', 'NgTableParams', '$window', function ($scope, $http, NgTableParams, $window) {

	// Method to retrieve Patient Data Server Side.
	$http({
		method: 'GET',
		url: '/getPatients'
	}).then(function successCallback(response) {

		// Debug.
		// console.log(JSON.stringify(response, null, "\t"));

		// Initialise Columns.
		$scope.cols = [
			{ field: "prefix", title: "Title", sortable: "prefix", filter: { prefix: "text" }, show: true },
			{ field: "firstName", title: "First Name", sortable: "firstName", filter: { firstName: "text" }, show: true },
			{ field: "lastName", title: "Last Name", sortable: "lastName", filter: { lastName: "text" }, show: true },
			{ field: "gender", title: "Gender", sortable: "gender", filter: { gender: "text" }, show: true },
			{ field: "dateOfBirth", title: "Date of Birth", sortable: "dateOfBirth", filter: { dateOfBirth: "text" }, show: true }
		];

		// Inject Parameters of ngTable and populate from http data.
		$scope.tableParams = new NgTableParams({
			page: 1, 						// show first page
			count: 10, 						// count per page
			// sorting: {firstName: "asc"},	// Initialise ascending sort.
		}, {
			dataset: response.data.content
		});

	}, function errorCallback(response) {
		// called asynchronously if an error occurs
		// or server returns response with an error status.
	});

	// Method for clicking individual patient to show new window.
	$scope.getPatient = function (patient) {

		// Open window to new URL and send JSON data.
		var $popup = $window.open('/displayPatient', "popup", "width=800,height=800");
		$popup.patient = patient;

	};

	// Method for logging out.
	 $scope.logout = function () {

		console.log("Logged out!");
		$window.location.href = '/logout';

	};


}]);

// Configure ngTable
(function() {
  "use strict";

  angular.module("AngularFlask").run(configureDefaults);
  configureDefaults.$inject = ["ngTableDefaults"];

  function configureDefaults(ngTableDefaults) {
    ngTableDefaults.params.count = 5;
    ngTableDefaults.settings.counts = [];
  }
})();