// Patient App and Controller for popup window.
var app = angular.module('patientApp', ['ngRoute'])
app.controller('patientController', function ($scope, $window) {

    // Method to retrieve telephone number
	$scope.test = function () {
        alert("Yes!");
	}

    // Define ng-bind="patientData" as string data.
    $scope.patientData = JSON.stringify($window.patient, null, "\t");
    $scope.patient = $window.patient;

    // Define binding for name.
    $scope.patientName = $window.patient.prefix 
    + " " + $window.patient.firstName 
    + " " + $window.patient.lastName;

});