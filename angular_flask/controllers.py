# Imports.
import os
import requests

from flask import Flask, request, Response, jsonify 
from flask import render_template, url_for, redirect, send_from_directory
from flask import send_file, make_response, abort, flash, session

from angular_flask import app

# Homepage route.
@app.route('/')
def home():

    # Check if user has logged in. If so, return index page.
    if not session.get('logged_in'):
        return render_template('login.html')

    else:
        return make_response(open('angular_flask/templates/index.html').read())

#Route for logging in.
@app.route('/login', methods=['POST'])
def login():

    # Call External API using post command.
    username = request.form['username']
    password = request.form['password']
    path = "https://auth.healthforge.io/auth/realms/interview/protocol/openid-connect/token"
    payload = "username=" + username + "&password=" + password + "&grant_type=password&client_id=interview"
    headers = {'content-type': "application/x-www-form-urlencoded"}
    loginRequest = requests.post(url=path, data=payload, headers=headers)
    loginJSON = loginRequest.json()

    # If status code = 200, then successful auth has been accepted by the farm.
    if loginRequest.status_code == 200:
        session['logged_in'] = True
        session["username"] = username
        session["password"] = password
        session["access_token"] = loginJSON["access_token"]

    else:
        session['failed_login'] = True
        flash('Wrong authentication!')

    # Return home route.
    return home()

# Route into external API.
@app.route('/getPatients')
def get_patients():

    # Check if user has logged in. If so, return patient data.
    if not session.get('logged_in'):
        return render_template('login.html')

    else:
        # Make external request to HealthForge API.
        url = "https://api.interview.healthforge.io:443/api/secure/patient"
        params = {"size": 500}
        headers = {'Accept': "application/json", "Authorization": "Bearer " + session["access_token"]}
        req = requests.get(url=url, headers=headers, params=params)

        ## Use jsonify to encode data.
        return jsonify(req.json())

# Route into patient page.
@app.route('/displayPatient')
def display_patient():
    
    # Check if user has logged in. If so, return individual patient data.
    if not session.get('logged_in'):
        return render_template('login.html')

    else:
        return render_template('userData.html')

# Route for logging out.
@app.route("/logout")
def logout():
    session['logged_in'] = False
    session['failed_login'] = False
    return render_template('login.html')

# special file handlers and error handlers
@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'img/favicon.ico')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
