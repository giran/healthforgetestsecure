# HealthForge API Test - Secure Portal

This repo serves as the submission to the second requirement of the technical test.
A custom login page has been created which uses direct grants to retrieve the required access token for the API.

### How to Get Started:

1. Clone this repository to the desired location.

2. Install all the necessary packages - ideally inside of a virtual environment.
> pip install -r requirements.txt

3. Run 'runserver.py' to start the web application.
> python runserver.py

4. Login to the portal with the correct credentials to view the patient data.
> http://localhost:4444/

#### Notes:
- The basis for this web app was provided by https://github.com/shea256/angular-flask but has been extensively modified for the requirements of the test.
- Incorrect credentials will prevent the end user from accessing the secure data through the custom login page.
- For efficiency, we have restricted the downloading of patient data to 500 records. This can be changed in controllers.py
- The backend is powered by Flask while the frontend is served through AngularJS and Bootstrap.