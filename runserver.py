#!/usr/bin/env python2.7
import os
from angular_flask import app

# Main function to start the webserver.
def runserver():
    port = int(os.environ.get('PORT', 4444))
    app.secret_key = os.urandom(12)
    app.run(host='localhost', port=port)

if __name__ == '__main__':
    runserver()